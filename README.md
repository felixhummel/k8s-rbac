# Create Unprivileged User and create kubectl config
- https://kubernetes.io/docs/reference/access-authn-authz/authentication/#service-account-tokens
- https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#config
```
# create stuff
ns=alpha

kubectl create namespace $ns
kubectl -n $ns create serviceaccount foo
kubectl -n $ns get serviceaccount

# fetch stuff
secret_id=$(
  kubectl -n $ns get serviceaccount/foo -ojson \
  | jq -r '.secrets[0].name'
)

server=$(kubectl config view -o jsonpath='{.clusters[0].cluster.server}')

ca_b64=$(
  kubectl -n $ns get secret/${secret_id} \
  -o jsonpath="{.data.ca\.crt}"
)
token=$(
  kubectl -n $ns get secret/${secret_id} \
  -o jsonpath="{.data.token}" \
  | base64 -d
)

# generate config
cat <<EOF > ~/.kube/foo.yml
apiVersion: v1
kind: Config
clusters:
- cluster:
    certificate-authority-data: $ca_b64
    server: $server
  name: foocluster
users:
- name: foo
  user:
    token: $token
contexts:
- context:
    cluster: foocluster
    namespace: $ns
    user: foo
  name: foo
current-context: foo
EOF
```

Try it
```
export KUBECONFIG=~/.kube/foo.yml
kubectl get pod
```
results in
```
Error from server (Forbidden): pods is forbidden: User "system:serviceaccount:alpha:foo" cannot list resource "pods" in API group "" in the namespace "alpha"
```
Woohooo! \o/


# Grant a few Permissions
```
kubectl apply -f kube/pod-read.yml
```

Try it
```
export KUBECONFIG=~/.kube/foo.yml
kubectl get pod
```
results in
```
No resources found.
```
Woohooo! \o/

The service account `foo` still cannot access pods in the `default` namespace,
as desired:
```
$ kubectl -n default get pod
Error from server (Forbidden): pods is forbidden: User "system:serviceaccount:alpha:foo" cannot list resource "pods" in API group "" in the namespace "default"
```


# Interlude: Python Client
```
pip install kubernetes
./list-pods.py
```

- https://github.com/kubernetes-client/python
- https://github.com/kubernetes-client/python/tree/master/examples
- https://github.com/kubernetes-client/python/blob/master/kubernetes/docs/CoreV1Api.md#list_namespaced_pod
