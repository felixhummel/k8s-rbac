#!/usr/bin/env python
# encoding: utf-8
from kubernetes import client, config

config.load_kube_config()

v1 = client.CoreV1Api()
ret = v1.list_namespaced_pod('alpha', watch=False)

if len(ret.items) < 1:
    print('no pods found')
    raise SystemExit

for i in ret.items:
    print(i.status.pod_ip, i.metadata.namespace, i.metadata.name)

