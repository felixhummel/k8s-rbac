People:

- alice
- bob
- charlie
- mallory

Groups (and members):

- developers (alice, bob) may create things
- reporters (charlie) may look at things that developers did
- baddies (mallory) may not do anything
